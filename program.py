# importing numpy module
import numpy as np

# importing matplotlib module
import matplotlib.pyplot as plt

# importing sys module
import sys

# check argument which is a file name
if len(sys.argv) == 1:
	print "input file name"
	exit(1)

# open author list file
file_name = sys.argv[1]
with open(file_name, 'r') as f:
	lines = f.read().splitlines()

# make elements list
# format: [idx, author, year]
idx = 0
elements = []
tmp = []
authors = []
year = 0
for line in lines:
	# line for indexing (numbering)
	if line.startswith("NO  - "):
		idx = int(line.strip("NO  - "))

	# line for author(s)
	if line.startswith("AU  - "):
		authors.append(line.strip("AU  - "))

	# line for publication year
	if line.startswith("PY  - "):
		year = int(line.strip("PY  - ").strip("///"))
		for author in authors:
			tmp.append(idx)
			tmp.append(author)
			tmp.append(year)
			elements.append(tmp)
			tmp = []
		authors = []

# sorting elements by author
elementsByAuthor = sorted(elements, key=lambda l:l[1])
#print(elementsByAuthor)

# sorting elements by year
elementsByYear = sorted(elements, key=lambda l:l[2])
#print(elementsByYear)

# plotting by author
clustersByAuthor = []
tmp_clustser = []
tmp_author_idx = 1
tmp_author = ""
plot_color = 'b'
if file_name == "new_input.txt":
	plot_color = 'r'
for element in elementsByAuthor:
	if tmp_author != element[1]:
		if tmp_clustser != []:
			clustersByAuthor.append(tmp_clustser)
			tmp_clustser = []
		tmp_clustser.append(tmp_author_idx)
		tmp_author_idx = tmp_author_idx + 1
		tmp_clustser.append(1)
		tmp_author = element[1]
	else:
		tmp_clustser[1] = tmp_clustser[1] + 1
x = []
y = [0] * len(clustersByAuthor)
s = []
ss = []
for cluster in clustersByAuthor:
	x.append(cluster[0])
	if file_name == "new_input.txt":
		s.append(cluster[1] * 10 / 6)
		ss.append((cluster[1] * 10 / 6) ** 2)
	else:
		s.append(cluster[1])
		ss.append(cluster[1] ** 2)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_ylim([-2, 20])
plt.scatter(x, y, color = plot_color, s=ss)
plt.plot(x, s, plot_color)
if file_name == "new_input.txt":
	plt.annotate('Li, L. & Littman, M. L.', xy=(60.5, 8), xytext=(20, 10.5), arrowprops=dict(facecolor='black', shrink=0.05), )
	plt.annotate('Sutton, R. S.', xy=(114, 15), xytext=(80, 17.5), arrowprops=dict(facecolor='black', shrink=0.05), )
else:
	plt.annotate('Bengio, Y.', xy=(10, 18), xytext=(45, 18.5), arrowprops=dict(facecolor='black', shrink=0.05), )
	plt.annotate('Hinton, G.', xy=(89, 15), xytext=(30, 12.5), arrowprops=dict(facecolor='black', shrink=0.05), )
	plt.annotate('LeCun, Y.', xy=(127, 17), xytext=(150, 17.5), arrowprops=dict(facecolor='black', shrink=0.05), )
#plt.show()

# plotting by year
clustersByYear = []
tmp_clustser = []
tmp_year = 0
for element in elementsByYear:
	if tmp_year != element[2]:
		if tmp_clustser != []:
			clustersByYear.append(tmp_clustser)
			tmp_clustser = []
		tmp_clustser.append(element[2])
		tmp_clustser.append(1)
		tmp_year = element[2]
	else:
		tmp_clustser[1] = tmp_clustser[1] + 1
x = []
y = [0] * len(clustersByYear)
s = []
ss = []
for cluster in clustersByYear:
	x.append(cluster[0])
	if file_name == "new_input.txt":
		s.append(cluster[1] * 10 / 6)
		ss.append((cluster[1] * 10 / 6) * 10)
	else:
		s.append(cluster[1])
		ss.append(cluster[1] * 10)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim([1930, 2020])
ax.set_ylim([-10, 110])		
plt.scatter(x, y, color = plot_color, s=ss)
plt.plot(x, s, plot_color)
if file_name == "new_input.txt":
	plt.annotate('y: 2009', xy=(2009, 45), xytext=(1995, 55), arrowprops=dict(facecolor='black', shrink=0.05), )
else:
	plt.annotate('y: 2014', xy=(2014, 99), xytext=(2000, 95), arrowprops=dict(facecolor='black', shrink=0.05), )
plt.show()


with open("output.txt", 'w') as f:
	for element in elements:
		data = "%s\t%s\t%s\n" % (element[0], element[1], element[2])
		f.write(data)
