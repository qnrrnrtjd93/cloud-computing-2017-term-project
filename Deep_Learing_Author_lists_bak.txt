NO  - 1
AU  - Krizhevsky, A.
AU  - Sutskever, I.
AU  - Hinton, G.
PY  - 2012///

NO  - 2
AU  - Farabet, C.
AU  - Couprie, C.
AU  - Najman, L.
AU  - LeCun, Y.
PY  - 2013///

NO  - 3
AU  - Tompson, J.
AU  - Jain, A.
AU  - LeCun, Y.
AU  - Bregler, C.
PY  - 2014///

NO  - 4
AU  - Szegedy, C.
AU  - Liu, W.
AU  - Jia, Y.
AU  - Sermanet, P.
AU  - Reed, S.
AU  - Anguelov, D.
AU  - Erhan, D.
AU  - Vanhoucke, V.
AU  - Rabinovich, A.
PY  - 2014///

NO  - 5
AU  - Mikolov, T.
AU  - Deoras, A.
AU  - Povey, D.
AU  - Burget, L.
AU  - Cernocky, J.
PY  - 2011///

NO  - 6
AU  - Hinton, G.
AU  - Deng, L.
AU  - Yu, D,
AU  - George E. Dahl, G. E. 
AU  - Mohamed, A.-R.
AU  - Jaitly, N.
AU  - Senior, A.
AU  - Vanhoucke, V.
AU  - Nguyen, P.
AU  - Sainath, T.
AU  - Kingsbury, B
PY  - 2012///

NO  - 7
AU  - Sainath, T.
AU  - Mohamed, A.-R.
AU  - Kingsbury, B.
AU  - Ramabhadran, B.
PY  - 2013///

NO  - 8
AU  - Ma, J.
AU  - Sheridan, R. P.
AU  - Liaw, A.
AU  - Dahl, G. E.
AU  - Svetnik, V.
PY  - 2015///

NO  - 9
AU  - Ciodaro, T.
AU  - Deva, D.
AU  - de Seixas, J.
AU  - Damazio, D.
PY  - 2012///

NO  - 10
AU  - Kaggle
PY  - 2014///

NO  - 11
AU  - Helmstaedter, M.
AU  - Briggman, K. L.
AU  - Turaga, S. C.
AU  - Jain, V.
AU  - Seung H. S.
AU  - Denk, W.
PY  - 2013///

NO  - 12
AU  - Leung, M. K.
AU  - Xiong, H. Y.
AU  - Lee, L. J.
AU  - Frey, B. J.
PY  - 2014///

NO  - 13
AU  - Xiong, H, Y,
AU  - Alipanahi, B.
AU  - Lee, L. J.
AU  - Bretschneider, H.
AU  - Merico, D.
AU  - Yuen, R. K. C.
AU  - Hua, Y.
AU  - Gueroussov, S.
AU  - Najafabadi, H. S.
AU  - Hughes, T. R.
AU  - Morris, Q.
AU  - Barash, Y.
AU  - Krainer, A. R.
AU  - Jojic, N.
AU  - Scherer, S. W.
AU  - Blencowe, B. J.
AU  - Frey, B. J.
PY  - 2015///

NO  - 14
AU  - Collobert, R.
AU  - Weston, J.
AU  - Bottou, L.
AU  - Karlen, M.
AU  - Kavukeuglu, K.
AU  - Kuksa, P.
PY  - 2011///

NO  - 15
AU  - Bordes, A.
AU  - Chopra, S.
AU  - Weston, J.
PY  - 2014///

NO  - 16
AU  - Jean, S.
AU  - Cho, K.
AU  - Memisevic, R.
AU  - Bengio, Y.
PY  - 2015///

NO  - 17
AU  - Sutskever, I.
AU  - Vinyals, O.
AU  - Le, Q. V.
PY  - 2014///

NO  - 18
AU  - Bottou, L.
AU  - Bousquet, O.
PY  - 2007///

NO  - 19
AU  - Duda, R. O.
AU  - Hart, P. E.
PY  - 1973///

NO  - 20
AU  - Scholkopf, B.
AU  - Smola, A.
PY  - 2002///

NO  - 21
AU  - Bengio, Y.
AU  - Delalleau, O.
AU  - Le Roux, N.
PY  - 2005///

NO  - 22
AU  - Selfridge, O. G.
PY  - 1958///

NO  - 23
AU  - Rosenblatt, F.
PY  - 1957///

NO  - 24
AU  - Werbos, P.
PY  - 1974///

NO  - 25
AU  - Parker, D. B.
PY  - 1985///

NO  - 26
AU  - LeCun, Y.
PY  - 1985///

NO  - 27
AU  - Rumelhart, D. E.
AU  - Hinton, G. E.
AU  - Williams, R. J.
PY  - 1986///

NO  - 28
AU  - Glorot, X.
AU  - Bordes, A.
AU  - Bengio, Y.
PY  - 2011///

NO  - 29
AU  - Dauphin, Y.
AU  - Pascanu, R.
AU  - Gulcehre, C.
AU  - Cho, K.
AU  - Ganguli, S.
AU  - Bengio, Y.
PY  - 2014///

NO  - 30
AU  - Choromanska, A.
AU  - Henaff, M.
AU  - Mathieu, M.
AU  - Arous, G. B.
AU  - LeCun, Y.
PY  - 2014///

NO  - 31
AU  - Hinton, G. E.
PY  - 2005///

NO  - 32
AU  - Hinton, G. E.
AU  - Osindero, S.
AU  - Teh, Y.-W.
PY  - 2006///

NO  - 33
AU  - Bengio, Y.
AU  - Lamblin, P.
AU  - Popovici, D.
AU  - Larochelle, H.
PY  - 2006///

NO  - 34
AU  - Ranzato, M.
AU  - Poultney, C.
AU  - Chopra, S.
AU  - LeCun, Y.
PY  - 2006///

NO  - 35
AU  - Hinton, G. E.
AU  - Salakhutdinov, R.
PY  - 2006///

NO  - 36
AU  - Sermanet, P.
AU  - Kavukcuoglu, K.
AU  - Chintala, S.
AU  - LeCun, Y.
PY  - 2013///

NO  - 37
AU  - Raina, R.
AU  - Madhavan, A.
AU  - Ng, A. Y.
PY  - 2009///

NO  - 38
AU  - Mohamed, A.-R.
AU  - Dahl, G. E.
AU  - Hinton, G.
PY  - 2012///

NO  - 39
AU  - Dahl, G. E.
AU  - Yu, D.
AU  - Deng, L.
AU  - Acero, A.
PY  - 2012///

NO  - 40
AU  - Bengio, Y.
AU  - Courville, A.
AU  - Vincent, P.
PY  - 2013///

NO  - 41
AU  - LeCun, Y.
AU  - Boser, B.
AU  - Denker, J. S.
AU  - Henderson, D.
AU  - Howard, R. E.
AU  - Hubbard, W.
AU  - Jackel, L. D.
PY  - 1990///

NO  - 42
AU  - LeCun, Y.
AU  - Bottou, L.
AU  - Bengio, Y.
AU  - Haffner, P.
PY  - 1998///

NO  - 43
AU  - Hubel, D. H.
AU  - Wiesel, T. N.
PY  - 1962///

NO  - 44
AU  - Felleman, D. J.
AU  - Essen, D. C. V.
PY  - 1991///

NO  - 45
AU  - Cadieu, C. F.
AU  - Hong, H.
AU  - Yamins, D. L. K.
AU  - Pinto, N.
AU  - Ardila, D.
AU  - Solomon, E. A.
AU  - Majaj, N. J.
AU  - DiCarlo, J. J.
PY  - 2014///

NO  - 46
AU  - Fukushima, K.
AU  - Miyake, S.
PY  - 1982///

NO  - 47
AU  - Waibel, A.
AU  - Hanazawa, T.
AU  - Hinton, G. E.
AU  - Shikano, K.
AU  - Lang, K.
PY  - 1989///

NO  - 48
AU  - Bottou, L.
AU  - Fogelman-Soulie, F.
AU  - Blanchet, P.
AU  - Lienard, J.
PY  - 1989///

NO  - 49
AU  - Simard, D.
AU  - Steinkraus, P. Y.
AU  - Platt, J. C.
PY  - 2003///

NO  - 50
AU  - Vaillant, R.
AU  - Monrocq, C.
AU  - LeCun, Y.
PY  - 1994///

NO  - 51
AU  - Nowlan, S.
AU  - Platt, J.
PY  - 1995///

NO  - 52
AU  - Lawrence, S.
AU  - Giles, C. L.
AU  - Tsoi, A. C.
AU  - Back, A. D.
PY  - 1997///

NO  - 53
AU  - Ciresan, D.
AU  - Meier, U.
AU  - Masci, J.
AU  - Schmidhuber, J.
PY  - 2012///

NO  - 54
AU  - Ning, F.
AU  - Delhomme, D.
AU  - LeCun, Y.
AU  - Piano, F.
AU  - Bottou, L.
AU  - Barbano, P. E.
PY  - 2005///

NO  - 55
AU  - Turaga, S. C.
AU  - Murray, J. F.
AU  - Jain, V.
AU  - Roth, F.
AU  - Helmstaedter, M.
AU  - Briggman, K.
AU  - Denk, W.
AU  - Seung, H. S.
PY  - 2010///

NO  - 56
AU  - Garcia, C.
AU  - Delakis, M.
PY  - 2004///

NO  - 57
AU  - Osadchy, M.
AU  - LeCun, Y.
AU  - Miller, M.
PY  - 2007///

NO  - 58
AU  - Tompson, J.
AU  - Goroshin, R. R.
AU  - Jain, A.
AU  - LeCun, Y. Y.
AU  - Bregler, C. C.
PY  - 2014///

NO  - 59
AU  - Taigman, Y.
AU  - Yang, M.
AU  - Ranzato, M.
AU  - Wolf, L.
PY  - 2014///

NO  - 60
AU  - Hadsell, R.
AU  - Sermanet, P.
AU  - Ben, J.
AU  - Erkan, A.
AU  - Scoffier, M.
AU  - Kavukcuoglu, K.
AU  - Muller, U.
AU  - LeCun, Y.
PY  - 2009///

NO  - 61
AU  - Farabet, C.
AU  - Couprie, C.
AU  - Najman, L.
AU  - LeCun, Y.
PY  - 2012///

NO  - 62
AU  - Srivastava, N.
AU  - Hinton, G.
AU  - Krizhevsky, A.
AU  - Sutskever, I.
AU  - Salakhutdinov, R.
PY  - 2014///

NO  - 63
AU  - Sermanet, P.
PY  - 2014///

NO  - 64
AU  - Girshick, R.
AU  - Donahue, J.
AU  - Darrell, T.
AU  - Malik, J.
PY  - 2014///

NO  - 65
AU  - Simonyan, K.
AU  - Zisserman, A.
PY  - 2014///

NO  - 66
AU  - Boser, B.
AU  - Sackinger, E.
AU  - Bromley, J.
AU  - LeCun, Y.
AU  - Jackel, L.
PY  - 1991///

NO  - 67
AU  - Farabet, C.
AU  - LeCun, Y.
AU  - Kavukcuoglu, K.
AU  - Culurciello, E.
AU  - Martini, B.
AU  - Akselrod, P.
AU  - Talay, S.
PY  - 2011///

NO  - 68
AU  - Bengio, Y.
PY  - 2009///

NO  - 69
AU  - Montufar, G.
AU  - Morton, J.
PY  - 2014///

NO  - 70
AU  - Montufar, G. F.
AU  - Pascanu, R.
AU  - Cho, K.
AU  - Bengio, Y.
PY  - 2014///

NO  - 71
AU  - Bengio, Y.
AU  - Ducharme, R.
AU  - Vincent, P.
PY  - 2001///

NO  - 72
AU  - Cho, K.
AU  - van Merrienboer, B.
AU  - Gulcehre, C.
AU  - Bahdanau, D.
AU  - Bougares, F.
AU  - Schwenk, H.
AU  - Bengio, Y.
PY  - 2014///

NO  - 73
AU  - Schwenk, H.
PY  - 2007///

NO  - 74
AU  - Socher, R.
AU  - Lin, C. C-Y.
AU  - Manning, C.
AU  - Ng, A. Y.
PY  - 2011///

NO  - 75
AU  - Mikolov, T.
AU  - Sutskever, I.
AU  - Chen, K.
AU  - Corrado, G.
AU  - Dean, J.
PY  - 2013///

NO  - 76
AU  - Bahdanau, D.
AU  - Cho, K.
AU  - Bengio, Y.
PY  - 2015///

NO  - 77
AU  - Hochreiter, S.
PY  - 1991///

NO  - 78
AU  - Bengio, Y.
AU  - Simard, P.
AU  - Frasconi, P.
PY  - 1994///

NO  - 79
AU  - Hochreiter, S.
AU  - Schmidhuber, J.
PY  - 1997///

NO  - 80
AU  - ElHihi, S.
AU  - Bengio, Y.
PY  - 1995///

NO  - 81
AU  - Sutskever, I.
PY  - 2012///

NO  - 82
AU  - Pascanu, R.
AU  - Mikolov, T.
AU  - Bengio, Y.
PY  - 2013///

NO  - 83
AU  - Sutskever, I.
AU  - Martens, J.
AU  - Hinton, G. E.
PY  - 2011///

NO  - 84
AU  - Lakoff, G.
AU  - Johnson, M.
PY  - 2008///

NO  - 85
AU  - Rogers, T. T.
AU  - McClelland, J. L.
PY  - 2004///

NO  - 86
AU  - Xu, K.
AU  - Ba, J.
AU  - Kiros, R.
AU  - Cho, K.
AU  - Courville, A.
AU  - Salakhutdinov, R.
AU  - Zemel, R.
AU  - Bengio, Y.
PY  - 2015///

NO  - 87
AU  - Graves, A.
AU  - Mohamed, A.-R.
AU  - Hinton, G.
PY  - 2013///

NO  - 88
AU  - Graves, A.
AU  - Wayne, G.
AU  - Danihelka, I.
PY  - 2014///

NO  - 89
AU  - Weston, J.
AU  - Chopra, S.
AU  - Bordes, A.
PY  - 2014///

NO  - 90
AU  - Weston, J.
AU  - Bordes, A.
AU  - Chopra, S.
AU  - Mikolov, T.
PY  - 2015///

NO  - 91
AU  - Hinton, G. E.
AU  - Dayan, P.
AU  - Frey, B. J.
AU  - Neal, R. M.
PY  - 1995///

NO  - 92
AU  - Salakhutdinov, R.
AU  - Hinton, G.
PY  - 2009///

NO  - 93
AU  - Vincent, P.
AU  - Larochelle, H.
AU  - Bengio, Y.
AU  - Manzagol, P.-A.
PY  - 2008///

NO  - 94
AU  - Kavakcuoglu, k.
AU  - Sermanet, P.
AU  - Boureau, Y. -L.
AU  - Gregor, K.
AU  - Mathieu, M.
AU  - LeCun, Y.
PY  - 2010///

NO  - 95
AU  - Gregor, K.
AU  - LeCun, Y.
PY  - 2010///

NO  - 96
AU  - Ranzato, M.
AU  - Mnih, V.
AU  - Susskind, J. M.
AU  - Hinton, G. E.
PY  - 2013///

NO  - 97
AU  - Bengio, Y.
AU  - Thibodeau-Laufer, E.
AU  - Alain, G.
AU  - Yosinski, J.
PY  - 2014///

NO  - 98
AU  - Kingma, D.
AU  - Rezende, D.
AU  - Mohamed, S.
AU  - Welling, M.
PY  - 2014///

NO  - 99
AU  - Ba, J.
AU  - Mnih, V.
AU  - Kavukcuoglu, K.
PY  - 2014///

NO  - 100
AU  - Mnih, V.
AU  - Kavukcuoglu, K.
AU  - Silver, D.
AU  - Rusu, A. A.
AU  - Veness, J.
AU  - Bellemare, M. G.
AU  - Graves, A.
AU  - Riedmiller, M.
AU  - Fidjeland, A. K. 
AU  - Ostrovski, G.
AU  - Petersen, S.
AU  - Beattie, C.
AU  - Sadik, A.
AU  - Antonoglou, I.
AU  - King, H.
AU  - Kumaran, D.
AU  - Wierstra, D. 
AU  - Leg, S.
AU  - Hassabis, D.
PY  - 2015///

NO  - 101
AU  - Bottou, L.
PY  - 2014///

NO  - 102
AU  - Vinyals, O.
AU  - Toshev, A.
AU  - Bengio, S.
AU  - Erhan, D.
PY  - 2014///

NO  - 103
AU  - van der Maaten, L.
AU  - Hinton, G. E.
PY  - 2008///